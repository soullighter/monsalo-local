<div class="go_up">
		<i class="fas fa-chevron-up"></i>
	</div>
	<div class="modal" id="login_modal">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<i class="far fa-times-circle modal_close" data-dismiss="modal"></i>
				<div class="modal-body">
					<div class="modal_top_title"><?php esc_html_e( 'please enter you information', 'monsalo' ); ?></div>
					<div class="modal_title"><?php esc_html_e( 'Login', 'monsalo' ); ?></div>
					<div class="custom-form">
						<?php echo do_shortcode( '[wc_login_form_monsalo]' ); ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="auth_modal">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<i class="far fa-times-circle modal_close" data-dismiss="modal"></i>
				<div class="modal-body">
					<div class="modal_top_title"><?php esc_html_e( 'please enter you information', 'monsalo' ); ?></div>
					<div class="modal_title"><?php esc_html_e( 'Register', 'monsalo' ); ?></div>
					<div class="custom-form">
						<?php echo do_shortcode( '[wc_reg_form_monsalo]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="reset_modal">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<i class="far fa-times-circle modal_close" data-dismiss="modal"></i>
				<div class="modal-body">
					<div class="modal_title">Lost password</div>
					<div class="custom-form">
						<?php echo do_shortcode( '[wc_lost_pass_monsalo]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="footer_logo col-md-6 col-lg-3 order-1 order-md-4 order-lg-0">
					<a class="logo" href="index.html">Dist<span>Price</span></a>
					<div>Discover the best products on DistPrice, curated by a community with great taste.</div>
				</div>
				
				<div class="footer_item col-md-4 col-lg-2 order-2 order-lg-0">
					<div class="footer_item_title">Resources</div>
					<ul class="footer_item_list">
						<li><a href="<?php echo get_template_directory_uri(); ?>/blog/">Blog</a></li>
						<li><a href="<?php echo get_template_directory_uri(); ?>/faq">FAQ</a></li>
					</ul>
				</div>
				<div class="footer_item col-md-4 col-lg-2 order-3 order-lg-0">
					<div class="footer_item_title">Company</div>
					<ul class="footer_item_list">
						<li><a href="<?php echo get_template_directory_uri(); ?>/privacy-policy-2/">Privacy Policy</a></li>
						<li><a href="<?php echo get_template_directory_uri(); ?>/terms-of-service/">Terms of Service</a></li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-3 order-5 order-lg-0 push-right">
					<div class="social">
						<div class="social_title">Follow us</div>
						<div class="d-flex justify-content-center justify-content-lg-start">
							<a class="social_link" href="#"><i class="fab fa-facebook-f"></i></a>
							<a class="social_link" href="#"><i class="fab fa-instagram"></i></a>
							<a class="social_link" href="#"><i class="fab fa-twitter"></i></a>
							<a class="social_link" href="#"><i class="fab fa-youtube"></i></a>
							<a class="social_link" href="#"><i class="fab fa-pinterest-p"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		

	</body>
</html>
