<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

// Snippet code goes here!
if (!current_user_can('administrator')) :
  show_admin_bar(false);
endif;

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // WooCommerce theme support
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );

    add_theme_support('woocommerce');

    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => 'menu',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '%3$s',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        // wp_register_script('popper', get_template_directory_uri() . '/lib/popper.js', array(), '2.7.1'); // popper
        // wp_enqueue_script('popper'); // Enqueue it!
        
        wp_register_script('select2-js', get_template_directory_uri() . '/js/select2.min.js', array(), '4.3.0', true); // Conditionizr
        wp_enqueue_script('select2-js'); // Enqueue it!

        wp_register_script('select-init', get_template_directory_uri() . '/js/select-init.js', array(), '4.3.0', true); // Conditionizr
        wp_enqueue_script('select-init'); // Enqueue it!

        wp_register_script('fontawesome-js', get_template_directory_uri() . '/fonts/fontawesome/js/all.min.js', array(), '4.3.0', true); // Conditionizr
        wp_enqueue_script('fontawesome-js'); // Enqueue it!

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0', true); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1', true); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('popper', get_template_directory_uri() . '/js/lib/popper.js', array(), '2.7.1', true); // popper
        wp_enqueue_script('popper'); // Enqueue it!

        wp_register_script('bootstrap', get_template_directory_uri() . '/js/lib/bootstrap.min.js', array(), '2.7.1', true); // bootstrap
        wp_enqueue_script('bootstrap'); // Enqueue it!

        wp_register_script('material-js', get_template_directory_uri() . '/js/lib/material.min.js', array(), '2.7.1', true); // material-js
        wp_enqueue_script('material-js'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.1', true); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{

    // wp_register_style('woocommerce', get_template_directory_uri() . '/css/woocommerce.css', array(), '1.0', 'all');
    wp_enqueue_style('woocommerce'); // Enqueue it!

    // wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    // wp_enqueue_style('normalize'); // Enqueue it!
     
    wp_register_style('select2', get_template_directory_uri() . '/css/select2.min.css', array(), '1.0', 'all');
    wp_enqueue_style('select2'); // Enqueue it!

    wp_register_style('fontawesome', get_template_directory_uri() . '/fonts/fontawesome/css/all.min.css', array(), '1.0', 'all');
    wp_enqueue_style('fontawesome'); // Enqueue it!
    
    wp_register_style('material-css', get_template_directory_uri() . '/css/material.min.css', array(), '1.0', 'all');
    wp_enqueue_style('material-css'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '100.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

add_filter( 'excerpt_length', 'html5wp_custom_post', 999 );

    

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
//add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

// Enable ACF Options page
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page();
    
}



// add_action( 'woocommerce_before_add_to_cart_button', 'monsalo_print_cart_array' );
//add_action( 'woocommerce_checkout_init', 'monsalo_apply_membership' );

// add_action( 'woocommerce_before_cart', 'monsalo_print_cart_array' );
 
function monsalo_print_cart_array() {
$cart = WC()->cart->get_cart();

foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
    $_product = $values['data'];
    echo "<div>";
    echo "<div>One Product</div>";
    print_r($_product);
    echo "</div>";
}
    // global $product;
    // print_r($tm_field);
}
/*
* Automatically adding the product to the cart.
*/
add_action( 'woocommerce_check_cart_items', 'add_membership_product_to_cart', 10, 2 );
function add_membership_product_to_cart() {
    if( is_cart() || is_checkout() ) {
        // Get all member plans
        $has_sub = wcs_user_has_subscription( '', '', 'active' );
        $free_product_id = 25713;  // Product Id of the free product which will get added to cart
        
        if ( !$has_sub ) {
            $found      = false;
            $product_is_member = false;
            //check if product already in cart
            if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
                foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
                    $_product = $values['data'];
                    $_get_product_price_variation = $values['tmcartepo']['0']['key'];
                    if ( $_get_product_price_variation === 'Member price_0' ) {
                        $product_is_member = true;
                    }
                    if ( WC_Subscriptions_Product::is_subscription( $_product) ) {
                        $found = true;
                    }
                    
                    
                }
                // if product not found, add it
                if ($product_is_member ) {
                    if (! $found) {
                        WC()->cart->add_to_cart( $free_product_id );
                    }
                } else {
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
                        $_product = $values['data'];
                        if ( WC_Subscriptions_Product::is_subscription( $_product) )
                            WC()->cart->remove_cart_item( $cart_item_key );
                    }
                }
                // if ( ! $found )
                //     WC()->cart->add_to_cart( $free_product_id );
            }
        }
    }
}

function remove_membership_from_cart() {
    foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
        $_product = $values['data'];
        if ( WC_Subscriptions_Product::is_subscription( $_product) )
            WC()->cart->remove_cart_item( $cart_item_key );
    }
}
add_action( 'template_redirect', 'remove_membership_from_cart_check', 10, 2 );
function remove_membership_from_cart_check() {
       if ( sizeof( WC()->cart->get_cart() ) === 1 ) {
        remove_membership_from_cart();
    }
}
// Change Subscription price to 0 (zero) in cart
add_filter( 'woocommerce_cart_item_price', 'subscription_cart_item_price', 10, 3 );
function subscription_cart_item_price( $price, $cart_item, $cart_item_key ) {

    $_product_price = $cart_item[ 'data' ]->price;
    $symbol = get_woocommerce_currency_symbol( $args['currency'] );
    if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'data' ]) ) {
        $price = $symbol . "0";
    }

    return $price;
}

function excerpt_in_cart($cart_item_html, $product_data, $cart_item) {
global $_product;

$excerpt = get_the_excerpt($product_data['product_id']);
$excerpt = substr($excerpt, 0, 80);

if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'data' ]) ) {
    echo $cart_item_html . '<br><p class="shortDescription">' . $excerpt . '...' . '</p>';
}
}

add_filter('woocommerce_cart_item_name', 'excerpt_in_cart', 99, 3);


add_filter( 'woocommerce_cart_item_name', 'bbloomer_cart_item_category', 99, 3);
 
function bbloomer_cart_item_category( $name, $cart_item, $cart_item_key ) {
 
$product_item = $cart_item['data'];
$p_name = $cart_item['data']->name;
$excerpt = get_the_excerpt($cart_item['data']->get_id());
 
if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'data' ]) ) { $name .= $p_name . '</br><span class="cart-sub-subdesc">' . $excerpt . '</span>';
} else {
    $name .= $p_name;
}
 
return $name;
 
}

add_filter( 'woocommerce_cart_item_subtotal', 'subscription_cart_item_subtotal', 10, 3 );
function subscription_cart_item_subtotal( $subtotal, $cart_item, $cart_item_key ) {

    $symbol = get_woocommerce_currency_symbol( $args['currency'] );
    if ( WC_Subscriptions_Product::is_subscription( $cart_item[ 'data' ]) ) {
        $subtotal = $symbol . "0";
    }

    return $subtotal;
}

// Change text of Membership in My Account
function sv_members_area_discounts_section_title( $title ) {
    return __( 'Subscription', 'my-theme-text-domain' );
}
add_filter( 'wc_memberships_my_account_my_memberships_title', 'sv_members_area_discounts_section_title' );

// add_action( 'woocommerce_before_cart', 'monsalo_find_product_in_cart' );   
function monsalo_find_product_in_cart() {
 
   $product_id = 25713;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
        $_product = $values['data'];
        if(  WC_Subscriptions_Product::is_subscription( $_product) )
            $notice = 'Subscription is in the Cart!';
             wc_print_notice( $notice, 'notice' );
    }
  
 
}

add_filter( 'woocommerce_cart_item_subtotal', 'monsalo_if_coupon_slash_item_subtotal', 99, 3 );
 
function monsalo_if_coupon_slash_item_subtotal( $subtotal, $cart_item, $cart_item_key ){
global $woocommerce;
 
// Note: use your own coupon code here
$coupon_code = 'barmada'; 
 
if ( $woocommerce->cart->has_discount( $coupon_code )) {
 
// Note: apply your own coupon discount multiplier here
// In this case, it's a 99% discount, hence I multiply by 0.01
$newsubtotal = wc_price( $cart_item['data']->get_price() * 0.20 * $cart_item['quantity'] ); 
 
$subtotal = sprintf( '<s>%s</s> %s', $subtotal, $newsubtotal ); 
}
 
return $subtotal;
}

// Remove button that removes item from product
add_filter('woocommerce_cart_item_remove_link', 'customized_cart_item_remove_link', 20, 2 );
function customized_cart_item_remove_link( $button_link, $cart_item_key ){
    //SET HERE your specific products IDs
    $targeted_products_ids = array( 25713 );

    // Get the current cart item
    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    // If the targeted product is in cart we remove the button link
    if( in_array($cart_item['data']->get_id(), $targeted_products_ids) )
        $button_link = '';

    return $button_link;
}
// Remove quantity input from product
add_filter( 'woocommerce_cart_item_quantity', 'wc_cart_item_quantity', 10, 3 );
function wc_cart_item_quantity( $product_quantity, $cart_item_key, $cart_item ){
    $targeted_products_ids = array( 25713 );

     // Get the current cart item
    $cart_item = WC()->cart->get_cart()[$cart_item_key];

    // If the targeted product is in cart we remove the button link
    if( in_array($cart_item['data']->get_id(), $targeted_products_ids) )
        $product_quantity = '';
    return $product_quantity;
}



function bsc_wc_memberships_members_only_product_price() {

global $product;
$user = wp_get_current_user();



$id = $product->get_id();
$discount = get_post_meta( $id, 'member_price', true );
$price = $product->get_price();
$regular_price  = get_post_meta( $product->get_id(), '_regular_price', true ); 
$sale_price     = get_post_meta( $product->get_id(), '_sale_price', true );
// echo 'Member price: $'.$price - $discount;
// echo 'User ID '. $id;
// echo 'Discount '. $sale_price;
// echo 'Price '. $regular_price;
// print_r($product_id);

$user_id = get_current_user_id();

$args = array( 
    'status' => array( 'active', 'complimentary', 'pending' ),
);  

$active_memberships = wc_memberships_get_user_memberships( $user_id, $args );


}
// add_action( 'woocommerce_before_add_to_cart_button', 'bsc_wc_memberships_members_only_product_price' );


function sv_change_member_product_price_display( $price ) {
    // bail if Memberships isn't active
    if ( ! function_exists( 'wc_memberships' ) ) {
        return $price;
    }
    
    // get membership plans
    $plans = wc_memberships_get_membership_plans();
    $active_member = array();
    // check if the member has an active membership for any plan
    foreach ( $plans as $plan ) {
        $active = wc_memberships_is_user_active_member( get_current_user_id(), $plan );
        array_push( $active_member, $active );
    }
    
    // only proceed if the user has no active memberships
    if ( ! in_array( true, $active_member )  ) {
    
        // change price display if purchasing is restricted
        if ( wc_memberships_is_product_purchasing_restricted() ) {
            $price = 'Price for members only';
        } 
    }
    
    return $price;
}
// add_filter( 'woocommerce_get_price_html', 'sv_change_member_product_price_display' );
// add_filter( 'woocommerce_cart_item_price', 'sv_change_member_product_price_display' );

/**
* @snippet     Remove Default Sorting Option @ WooCommerce Shop
*/
add_filter( 'woocommerce_catalog_orderby', 'monsalo_remove_sorting_option_woocommerce_shop' );
 
function monsalo_remove_sorting_option_woocommerce_shop( $options ) {
   unset( $options['menu_order'] );   
   unset( $options['date'] );   
   unset( $options['price-desc'] );   
   return $options;
}

/**
* @snippet     Rename a Default Sorting Option @ WooCommerce Shop
*/
 
add_filter( 'woocommerce_catalog_orderby', 'monsalo_rename_sorting_option_woocommerce_shop' );
 
function monsalo_rename_sorting_option_woocommerce_shop( $options ) {
   $options['popularity'] = __( 'Popular', 'woocommerce' );   
   $options['rating'] = __( 'Top', 'woocommerce' );   
   $options['price'] = __( 'Price', 'woocommerce' );   
   return $options;
}
 
// Note: you can unset another default sorting option... here's the list:
// 'menu_order' => __( 'Default sorting', 'woocommerce' ),
// 'popularity' => __( 'Sort by popularity', 'woocommerce' ),
// 'rating'     => __( 'Sort by average rating', 'woocommerce' ),
// 'date'       => __( 'Sort by newness', 'woocommerce' ),
// 'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
// 'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),


/**
 * @snippet       WooCommerce User Registration Shortcode
 */
  
// THIS WILL CREATE A NEW SHORTCODE: [wc_reg_form_monsalo]
  
add_shortcode( 'wc_reg_form_monsalo', 'monsalo_separate_registration_form' );
    
function monsalo_separate_registration_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return;
   ob_start();
 
   // NOTE: THE FOLLOWING <FORM></FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
   // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY AND REPLACE CSS CLASSES!!!
    
   ?>
 
        <form method="post" class="modal_form" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="text" class="mdl-textfield__input" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                    <label for="reg_username" class="mdl-textfield__label"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                </div>

            <?php endif; ?>

            <div class="account_form_label mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input type="email" class="mdl-textfield__input" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                <label for="reg_email" class="mdl-textfield__label"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            </div>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input type="password" class="mdl-textfield__input pass" name="password" id="reg_password" autocomplete="new-password" />
                    <label for="reg_password" class="mdl-textfield__label"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <div class="show_pass"><?php esc_html_e( 'Show', 'woocommerce' ); ?></div>
                </div>

            <?php else : ?>

                <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

            <?php endif; ?>

            <?php do_action( 'woocommerce_register_form' ); ?>

            <div class="woocommerce-FormRow form-row">
                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                <button type="submit" class="modal_submit_btn" name="register" value="<?php esc_attr_e( 'Sing Up', 'woocommerce' ); ?>"><?php esc_html_e( 'Sing Up', 'woocommerce' ); ?></button>
            </div>

            <?php do_action( 'woocommerce_register_form_end' ); ?>
            <div class="modal_open_link" data-toggle="modal" data-target="#login_modal" data-dismiss="modal"><?php esc_attr_e( 'Login', 'monsalo' ); ?></div>
      </form>
 
   <?php
     
   return ob_get_clean();
}

/**
 * @snippet       WooCommerce User Login Shortcode
 */
  
// THIS WILL CREATE A NEW SHORTCODE: [wc_login_form_monsalo]
  
add_shortcode( 'wc_login_form_monsalo', 'monsalo_separate_login_form' );
  
function monsalo_separate_login_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return; 
   ob_start(); 
   ?>

    <form class="modal_form d-flex flex-column justify-content-center align-items-center" method="post">

        <?php do_action( 'woocommerce_login_form_start' ); ?>

      <div class="account_form_label mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input type="text" class="mdl-textfield__input" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
            <label for="username" class="mdl-textfield__label"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input class="mdl-textfield__input" type="password" name="password" id="password" autocomplete="current-password" />
            <label for="password" class="mdl-textfield__label"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
        </div>

        <?php do_action( 'woocommerce_login_form' ); ?>

        <div class="modal_links_wrap login_links d-flex justify-content-between">
            <div class="custom_checkboxes">
                <label class="input_container"><?php esc_html_e( 'Remember me', 'woocommerce' ); ?>
                    <input class="checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" />
                    <span class="checkmark"></span>
                </label>
            </div>
                <div class="modal_open_link" data-toggle="modal" data-target="#reset_modal" data-dismiss="modal"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></div>
        </div>
            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
            <button type="submit" class="modal_submit_btn" name="login" value="<?php esc_attr_e( 'Sign in', 'woocommerce' ); ?>"><?php esc_html_e( 'Sign in', 'woocommerce' ); ?></button>

        <?php do_action( 'woocommerce_login_form_end' ); ?>

    </form>
   <?php
   return ob_get_clean();
}

/**
 * @snippet       WooCommerce User My Account Shortcode
 */
  
// THIS WILL CREATE A NEW SHORTCODE: [wc_lost_pass_monsalo]
  
add_shortcode( 'wc_lost_pass_monsalo', 'monsalo_separate_my_account_form' );
  
function monsalo_separate_my_account_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return; 
   ob_start(); 
    do_action( 'woocommerce_before_lost_password_form' );
    ?>

    <form method="post" class="modal_form">

        <p><?php echo apply_filters( 'woocommerce_lost_password_message', esc_html__( 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p><?php // @codingStandardsIgnoreLine ?>

       <div class="account_form_label mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input class="mdl-textfield__input" type="text" name="user_login" id="user_login" autocomplete="username" />
            <label for="user_login" class="mdl-textfield__label"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?></label>
        </div>
        <div class="modal_links_wrap d-flex justify-content-between">
            <div class="modal_open_link" data-toggle="modal" data-target="#auth_modal" data-dismiss="modal">Sign up</div>
            <div class="modal_open_link" data-toggle="modal" data-target="#login_modal" data-dismiss="modal">Login</div>
        </div>

        <div class="clear"></div>

        <?php do_action( 'woocommerce_lostpassword_form' ); ?>

            <input type="hidden" name="wc_reset_password" value="true" />
            <button type="submit" class="modal_submit_btn" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Reset password', 'woocommerce' ); ?></button>


        <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>

    </form>
    <?php
    do_action( 'woocommerce_after_lost_password_form' );
   return ob_get_clean();
}
    
    /**
     * Show cart contents / total Ajax
     */
    add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

    function woocommerce_header_add_to_cart_fragment( $fragments ) {
        global $woocommerce;

        ob_start();

        ?>
       <span class="cart-customlocation cart-number"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
        <?php
        $fragments['span.cart-customlocation'] = ob_get_clean();
        return $fragments;
    }

    // Display JUST products in shop pages
    add_filter( 'woocommerce_product_query_tax_query', 'custom_product_query_tax_query', 10, 2 );
    function custom_product_query_tax_query( $tax_query, $query ) {
        if( is_admin() ) return $tax_query;

        if ( is_shop() ) {
            $tax_query[] = array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => 'hidden',
                'operator' => 'NOT IN'
            );
        }

        return $tax_query;
    }




function my_custom_function(){
        do_action("woocommerce_tm_epo_fields");
    }

    add_action( 'woocommerce_single_product_summary','my_custom_function');

/**
* @snippet       Hide Edit Address Tab @ My Account
*/
 
add_filter( 'woocommerce_account_menu_items', 'monsalo_remove_address_my_account', 999 );
 
function monsalo_remove_address_my_account( $items ) {
unset($items['downloads']);
return $items;
}

add_action( 'woocommerce_cart_actions', 'move_proceed_button' );
function move_proceed_button( $checkout ) {
    echo '<div class="additional-checkout-button"><a href="' . esc_url( WC()->cart->get_checkout_url() ) . '" class="checkout-button button alt wc-forward" >' . __( 'Proceed to Checkout', 'woocommerce' ) . '</a></div>';
}





