<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
      <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-180x180.png">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">


		<!-- Custom Browsers Color -->
		<meta name="theme-color" content="#2edab8">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

		<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,300i,400,600,700,800&display=swap" rel="stylesheet">
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">


		<header class="header active">
		<!-- <div class="header_top">
			<div class="container d-flex align-items-center justify-content-md-between justify-content-end h-100">
				<div class="header_top_text">Compare millions of products from hundreds of merchants all in one place.</div>

				<div class="user_menu d-flex align-items-center h-100 flex-shrink-0">

					<?php 
				    if ( is_user_logged_in() ) { 
			        echo '<div class="user_menu_item d-flex align-items-center justify-content-center h-100 login_btn"><a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">My Account</a></div>';
				    }
				    elseif ( !is_user_logged_in() ) {
							echo '<div class="user_menu_item d-flex align-items-center justify-content-center h-100 login_btn" data-toggle="modal" data-target="#login_modal">Login</div>';
							echo '<div class="user_menu_item d-flex align-items-center justify-content-center h-100 signup_btn" data-toggle="modal" data-target="#auth_modal">Signup</div>';
				    }

					 ?>

				</div>

				<div class="shop_menu d-flex align-items-center">
					<div class="favorites">
						<a href="<?php echo get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>"><i class="far fa-heart"></i></a>
					</div>
					<div class="cart-icon">
						<a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><i class="fas fa-shopping-bag"></i></a>
						<span class="cart-customlocation cart-number"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span>
					</div>

					<div class="burger">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div> -->
		<div class="header_content">
			<div class="container d-flex align-items-center justify-content-between h-100">
			<a class="logo" href="<?php echo home_url(); ?>">Dist<span>Price</span></a>

				<div class="d-flex align-items-center form-burger-wrap">
					<form class="search-form" method="get" action="<?php echo home_url(); ?>" role="search">
						<input class="search_input" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'html5blank' ); ?>">
						<input type="hidden" name="post_type" value="product" />
						<button class="search_btn search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
					</form>
					<div class="burger">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				


				<ul class="menu">
					<li class="page_item has_child">
						<a href="<?php echo home_url(); ?>">Products</a>
							<?php if ( have_rows( 'products_list', 'option' ) ) : ?>
							<ul class="sub_menu">
								<div class="sub_menu_content">
								<?php while ( have_rows( 'products_list', 'option' ) ) : the_row(); ?>
									<li class="sub_menu_item">
									<?php $catalog_type_term = get_sub_field( 'catalog_type' ); ?>
										<a href="<?php echo get_term_link( $catalog_type_term ); ?>"><?php echo $catalog_type_term->name; ?><img src="<?php echo get_template_directory_uri() ?>/img/two-arrows.png" alt=""></a>
												<?php if ( have_rows( 'submenu_category_list' ) ) : ?>
												<div class="sub_menu">
													<div class="sub_menu_content">
														<div class="row">
															<?php while ( have_rows( 'submenu_category_list' ) ) : the_row(); ?>
															<div class="col-lg-3 col-md-6">

																<?php if ( have_rows( 'menu_item' ) ) : ?>
																	<?php while ( have_rows( 'menu_item' ) ) : the_row(); ?>
																	<div class="sub_menu_list">
																		<?php $menu_item_name_term = get_sub_field( 'menu_item_name' ); ?>
																		<div class="sub_menu_title"><?php echo $menu_item_name_term->name; ?></div>

																		<?php $menu_item_categories_terms = get_sub_field( 'menu_item_categories' ); ?>
																		<?php if ( $menu_item_categories_terms ): ?>
																			<ul>
																			<?php foreach ( $menu_item_categories_terms as $menu_item_categories_term ): ?>
																				<li class="sub_menu_list_item"><a href="<?php echo get_term_link( $menu_item_categories_term ); ?>"><?php echo $menu_item_categories_term->name; ?></a></li>
																			<?php endforeach; ?>
																				<li class="sub_menu_list_item"><a class="view_all_btn" href="<?php echo get_term_link( $menu_item_name_term ); ?>">VIEW ALL</a></li>
																			</ul>
																		<?php endif; ?>

																	</div>
																	<?php endwhile; ?>
																<!-- if menu_item END -->
																<?php endif; ?>
															</div>
															<?php endwhile; ?>
														</div>
													</div>
												</div>
												<!-- if submenu_category_list end -->
												<?php endif; ?>
												
									</li>
								<?php endwhile; ?>
							
							</div>
						</ul>
							<!-- if products_list end -->
							<?php endif; ?>
					</li>
					<?php html5blank_nav(); ?>
					<li class="page_item menu_item">
						<?php 
					    if ( is_user_logged_in() ) { 
				        echo '<a href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">My Account</a>';
					    }
					    elseif ( !is_user_logged_in() ) {
								echo '<div data-toggle="modal" data-target="#login_modal">Login</div>';
					    }

						 ?>
					</li>
				</ul>
			</div>
		</div>
	</header>

    <div id="content" class="site-content">
