<?php
    /*
     Template Name: Blog
    */
    get_header();
?>
			
<main class="main blog-page">
	 
		<section class="container section">
			<div class="row">

         <div class="col-12">
         <div class="best_selling_title pb-4">Blog</div>
         </div>
            <?php

            $main_args = [
                'post_type' => 'post',
                
            ];

            $the_main_loop = new WP_Query($main_args);


            if($the_main_loop->have_posts()) { 
                while($the_main_loop->have_posts()) { 
                $the_main_loop->the_post(); 
            
                get_template_part( 'template-parts/content', 'page' );
            
                } // endwhile
                wp_reset_postdata(); // VERY VERY IMPORTANT
            }


            while ( have_posts() ) : the_post();

             

            endwhile; // End of the loop.
            ?>
         </div>

       

		</section>
</main>

<?php get_footer(); ?>