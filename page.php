<?php get_header(); ?>

	<main role="main" class="main grey-bg pb-4">
		<!-- section -->
		<section class="container section editor">
			<div class="row justify-content-center">
			<div class="col-12 col-xl-10 white-bg">
			<h1 class="best_selling_title"><?php the_title(); ?></h1>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php the_content(); ?>

						<?php comments_template( '', true ); // Remove if you don't want comments ?>

						<br class="clear">

						<?php edit_post_link(); ?>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

				<?php else: ?>

					<!-- article -->
					<article>

						<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

					</article>
					<!-- /article -->

				<?php endif; ?>

			</div>
			</div>
		
		
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
