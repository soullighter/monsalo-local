<?php get_header(); ?>

<main class="main grey-bg blog-single">
   <!-- section -->
   <section class="section editor">

      <div class="container blog-inner">
         <div class="row justify-content-center">
            <div class="col-12 col-xl-10 white-bg page-inner-padding">

               <?php if (have_posts()): while (have_posts()) : the_post(); ?>

               <!-- article -->
               <article id="post-<?php the_ID(); ?>">

                  <!-- post thumbnail -->
                  <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                  <?php $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
                  <div class="blog-hero-img cover" style="background-image: url(<?php echo $backgroundImg[0]?>)" ;>
                  </div>
                  <?php endif; ?>

                  <!-- /post thumbnail -->

                  <h1 class="best_selling_title pb-4"><?php the_title(); ?></h1>


                  <!-- /post title -->

                  <div class="blog-inner-content">
                     <?php the_content(); // Dynamic Content ?>
                  </div>



               </article>
               <!-- /article -->

               <?php endwhile; ?>

               <?php else: ?>

               <?php endif; ?>
            </div>
         </div>
		</div>
		

         <div class="container section">
            <div class="move-content container latest-news blog-page">
               <div class="row">
                  <h3 class="mb-4">Latest post</h3>
               </div>


               <?php get_template_part( 'template-parts/latest', 'news' ); ?>
            </div>
         </div>
   </section>





</main>





<?php get_footer(); ?>