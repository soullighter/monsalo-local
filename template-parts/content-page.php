<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

?>
	
<article id="post-<?php the_ID(); ?>" class="col-12 col-md-6 col-lg-4 col-xl-3 mb-5 ">
	<div class="blog-card">
		<a class="img-wrap-16-9" href="<?php echo get_permalink();?>">
			<?php ; 
				if (has_post_thumbnail()) {
					$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
					<img src="<?php echo $backgroundImg[0]?>" alt="Blog">
			<?php
				}
			?>
		</a>

		<div class="blog-content" >
         <a href="<?php echo get_permalink();?>"><?php the_title( '<h5 class="blog-title text-center">', '</h5>' ); ?></a>
         <hr>
         <a href="<?php echo get_permalink();?>"><p><?php the_excerpt(); ?></p></a>
         
         <a class="btn-link" href="<?php echo get_permalink();?>"><?php esc_html_e( 'Read more', 'funpark_theme' ) ?> &#10140;</a>
         </div><!-- .entry-content -->
     
         </div>
</article><!-- #post-## -->
