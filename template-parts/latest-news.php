<?php 
   // the query
   $the_query = new WP_Query( array(
   'post_type' => 'post',
      'posts_per_page' => 3,
   )); 
?>

<?php if ( $the_query->have_posts() ) : ?>

   <div class="row">

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
         
      <?php get_template_part( 'template-parts/content', 'page' ); ?>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

</div>
<!-- end latest-news -->
<?php else : ?>
<?php endif; ?>