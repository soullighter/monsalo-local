<?php /* Template Name: About */ get_header(); ?>

<main class="main">
	<section class="container-fluid grey-bg section">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-10 text-center">
				<h2>HOW DISTPRICE WORKS</h2>
				<h4>SAVE HUNDREDS OF POUNDS WITH DISTPRICE EVERY YEAR</h4>
				<h5 class="italic">DistPrice is a new and unique concept for members only. As a member you can save up to 80% on everything from exclusive <br> brands to well known everyday products.</h5>
				<p>Save money every time you shop for beauty products, cosmetics, perfume, hair products, natural and organic products, household items and much more. Try DistPrice for free for <span class="underline">one month</span>  and save money on items for yourself, friends, <span class="underline">colleagues or family</span>. After the free trial period, the price is <span class="mark">£8.99</span> per month, and you can cancel your membership at any time.</p>

				<br>
				<h5><strong>Commence your membership by completing a purchase, and start saving now!</strong></h5>

			</div>
		</div>
	</section>

	<section class="container-fluid section">
		<div class="row">
			<div class="col-12 col-xl-7 center">
				<div class="row benefits">
					<div class="col-12 text-center">
						<h2>DISTPRICE BENEFITS</h2>
					</div>
					<div class="col-12 col-sm-6 text-center">
						<div class="icon-wrap center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/precentage.png" alt="Precentage">
						</div>
						<h4>SAVE UP TO 80%</h4>
						<h6>DistPrice offers discounts of up to 80% <br> off on thousands of well known brands</h6>
					</div>
					<div class="col-12 col-sm-6 text-center">
						<div class="icon-wrap center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/box-delivery.png" alt="delivery">
						</div>
						<h4>CHEAP AND FAST DELIVERY</h4>
						<h6>We offer you cheap and speedy delivery within <br> 2-7 days as well as a 30 days return policy</h6>
					</div>
					<div class="col-12 col-sm-6 text-center">
						<div class="icon-wrap center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/currency.png" alt="Precentage">
						</div>
						<h4>EARN BONUSCASH</h4>
						<h6>Receive BonusCash every time you shop <br> and use on your next purchase</h6>
					</div>
					<div class="col-12 col-sm-6 text-center">
						<div class="icon-wrap center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/chest.png" alt="Precentage">
						</div>
						<h4>FIRST MONTH FREE</h4>
						<h6>Try DistPrice for 30 days, free of charge. To see <br> whether or not a membership is for you</h6>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="container-fluid grey-bg section" >
		<div class="row justify-centent-center orders-icon">
			<div class="col-12 text-center">
				<h2>THE MORE YOU BUY, THE MORE YOU SAVE.</h2>
				<p>The larger your order at luxplus, the more you can save.<br>See examples of order sizes and savings here!</p>
			</div>
			<div class="col-4 col-md-3 text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon.png" alt="small orders">
				<h4 class="uppercase">Small Orders</h4>
				<h6>Save $38/ <span> month</span></h6>
			</div>
			<div class="col-4 col-md-3 text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon2.png" alt="medium orders">
				<h4 class="uppercase">Small Orders</h4>
				<h6>Save $38/ <span> month</span></h6>
			</div>
			<div class="col-4 col-md-3 text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/img/icon3.png" alt="large orders">
				<h4 class="uppercase">Small Orders</h4>
				<h6>Save $38/ <span> month</span></h6>
			</div>
			<div class="col-12 text-center">
				<a href="#" class="btn-link underline">Read in detail <span class="arrow-right"></span></a>
			</div>
		</div>
	</section>

	<section class="container-fluid section offer">
		<div class="row text-center">
			<div class="col-12">
				<h2>WANT TO KNOW MORE?</h2>
				<p class="mark">We'd love to hear from you.</p>
				<p>We are constantly working to find new products and offers for our members. If you have any <br> questions or are looking for something in particular, just drop us a line! <br>You can contact us here:</p><br>

				<button class="additional_filter_btn uppercase center">Customer Service</button>
			</div>
		</div>
	</section>

	<div class="reviews_section">
		<div class="container">
			<div class="best_selling_title">Customer Reviews</div>
			<div class="d-flex justify-content-center flex-wrap">
				<div class="reviews_item">
					<div class="d-flex justify-content-between">
						<div class="reviews_name">jane</div>
						<div class="rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="reviews_text">The variety of products on DistPrice was amazing and so too were the prices. The delivery was quick!</div>
				</div>
				<div class="reviews_item">
					<div class="d-flex justify-content-between">
						<div class="reviews_name">jane</div>
						<div class="rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="reviews_text">The variety of products on DistPrice was amazing and so too were the prices. The delivery was quick!</div>
				</div>
				<div class="reviews_item">
					<div class="d-flex justify-content-between">
						<div class="reviews_name">jane</div>
						<div class="rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="reviews_text">The variety of products on DistPrice was amazing and so too were the prices. The delivery was quick!</div>
				</div>
				<div class="reviews_item">
					<div class="d-flex justify-content-between">
						<div class="reviews_name">jane</div>
						<div class="rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="reviews_text">The variety of products on DistPrice was amazing and so too were the prices. The delivery was quick!</div>
				</div>
				<div class="reviews_item">
					<div class="d-flex justify-content-between">
						<div class="reviews_name">jane</div>
						<div class="rating">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="reviews_text">The variety of products on DistPrice was amazing and so too were the prices. The delivery was quick!</div>
				</div>
			</div>
		</div>
	</div>

	<div class="subscribe_section">
		<div class="container d-flex justify-content-center flex-column align-items-center">
			<div class="subscribe_title">KEEP UP WITH NEW ARRIVALS AND TOP OFFERS</div>
			<div class="subscribe_subtitle">Sign up for the DistPrice newsletter.</div>
			<form class="subscribe" action="#">
				<input class="subscribe_input" type="text" required placeholder="Your email address">
				<button class="subscribe_btn" type="submit">Sign Up!</button>
			</form>
		</div>
	</div>

</main>

<?php get_footer(); ?>