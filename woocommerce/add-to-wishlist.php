<?php
/**
 * Add to wishlist template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.0
 */

if ( ! defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly

global $product;
?>

<div class="add-to-wishlist-<?php echo $product_id ?> prod_card_info_item prod_card_favorites d-flex align-items-center">
	<style type="text/css">
		.prod_card_favorites svg > g {
   		stroke: #f05228;
   	}
   	.prod_card_favorites svg {
		    height: 25px;
		    width: 25px;
		    transition: all 0.2s ease-in-out;
		}
		.woocommerce a.add_to_wishlist,
		.woocommerce a.remove_from_wishlist_custom {
			padding: 5px;
		}
		.woocommerce a.add_to_wishlist:hover,
		.woocommerce a.remove_from_wishlist_custom:hover,
		.woocommerce a.remove_from_wishlist_custom {
			background: transparent;
			border-color: transparent;
		}
		.woocommerce a.add_to_wishlist:hover svg g > path,
		.woocommerce a.remove_from_wishlist_custom svg g > path{
			fill: #f05228;
		}
		.woocommerce a.add_to_wishlist svg g > path{
			fill: #fff;
		}
		.woocommerce a.remove_from_wishlist_custom:hover svg g > path{
			fill: #fff;
		}
	</style>
	<?php if( ! ( $disable_wishlist && ! is_user_logged_in() ) ): ?>
	    <div class="yith-wcwl-add-button <?php echo ( $exists && ! $available_multi_wishlist ) ? 'hide': 'show' ?>" style="display:<?php echo ( $exists && ! $available_multi_wishlist ) ? 'none': 'block' ?>">

	        <a href="<?php echo esc_url( add_query_arg( 'add_to_wishlist', $product_id ) )?>" rel="nofollow" data-product-id="<?php echo $product_id ?>" data-product-type="<?php echo $product_type?>" class="<?php echo $link_classes ?>" >
					  
					    <svg width="107px" height="97px" viewBox="0 0 107 97" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"class="svg-inline--fa fa-heart fa-w-16 favor_icon" data-toggle="tooltip" data-placement="bottom" title="" aria-labelledby="svg-inline--fa-title-KZ3oYq1jJ4UC" data-prefix="far" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><title id="svg-inline--fa-title-KZ3oYq1jJ4UC">Add to favorites.</title>
                    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
                    <title>heart</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="heart" transform="translate(3.000000, 3.000000)" stroke-width="6">
                            <path d="M48.5,9.3 C48.5,9.3 71.3,-11.5 92,9.3 C92,9.3 108.3,24.8 95.3,45.5 C95.3,45.5 93,54.2 48.5,91.3" id="XMLID_3_"></path>
                            <path d="M52.5,9.3 C52.5,9.3 29.7,-11.5 9,9.3 C9,9.3 -7.3,24.8 5.7,45.5 C5.7,45.5 8,54.2 52.5,91.3" id="XMLID_4_"></path>
                        </g>
                    </g>
                </svg>
					    <?php echo $label ?>
					</a>
					<img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	        
	    </div>

	  <div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;">
	        <a class="remove_from_wishlist_custom" href="#" rel="nofollow" data-product-id="<?php echo $product_id ?>">
	        	
	             <svg width="107px" height="97px" viewBox="0 0 107 97" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"class="svg-inline--fa fa-heart fa-w-16 favor_icon" data-toggle="tooltip" data-placement="bottom" title="" aria-labelledby="svg-inline--fa-title-KZ3oYq1jJ4UC" data-prefix="far" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><title id="svg-inline--fa-title-KZ3oYq1jJ4UC">Remove from favorites.</title>
                    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
                    <title>heart</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="heart" transform="translate(3.000000, 3.000000)" stroke-width="6">
                            <path d="M48.5,9.3 C48.5,9.3 71.3,-11.5 92,9.3 C92,9.3 108.3,24.8 95.3,45.5 C95.3,45.5 93,54.2 48.5,91.3" id="XMLID_3_"></path>
                            <path d="M52.5,9.3 C52.5,9.3 29.7,-11.5 9,9.3 C9,9.3 -7.3,24.8 5.7,45.5 C5.7,45.5 8,54.2 52.5,91.3" id="XMLID_4_"></path>
                        </g>
                    </g>
                </svg>
	        </a>
		    <img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div class="yith-wcwl-wishlistexistsbrowse <?php echo ( $exists && ! $available_multi_wishlist ) ? 'show' : 'hide' ?>" style="display:<?php echo ( $exists && ! $available_multi_wishlist ) ? 'block' : 'none' ?>">
	        <a class="remove_from_wishlist_custom" href="#" rel="nofollow" data-product-id="<?php echo $product_id ?>">
		          <svg width="107px" height="97px" viewBox="0 0 107 97" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"class="svg-inline--fa fa-heart fa-w-16 favor_icon" data-toggle="tooltip" data-placement="bottom" title="" aria-labelledby="svg-inline--fa-title-KZ3oYq1jJ4UC" data-prefix="far" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><title id="svg-inline--fa-title-KZ3oYq1jJ4UC">Remove from favorites.</title>
                    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
                    <title>heart</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="heart" transform="translate(3.000000, 3.000000)" stroke-width="6">
                            <path d="M48.5,9.3 C48.5,9.3 71.3,-11.5 92,9.3 C92,9.3 108.3,24.8 95.3,45.5 C95.3,45.5 93,54.2 48.5,91.3" id="XMLID_3_"></path>
                            <path d="M52.5,9.3 C52.5,9.3 29.7,-11.5 9,9.3 C9,9.3 -7.3,24.8 5.7,45.5 C5.7,45.5 8,54.2 52.5,91.3" id="XMLID_4_"></path>
                        </g>
                    </g>
                </svg>
	        </a>
		    <img src="<?php echo esc_url( YITH_WCWL_URL . 'assets/images/wpspin_light.gif' ) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden" />
	    </div>

	    <div style="clear:both"></div>
	    <div class="yith-wcwl-wishlistaddresponse"></div>
	<?php else: ?>
		<a href="<?php echo esc_url( add_query_arg( array( 'wishlist_notice' => 'true', 'add_to_wishlist' => $product_id ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) )?>" rel="nofollow" class="<?php echo str_replace( 'add_to_wishlist', '', $link_classes ) ?>" >
			<?php echo $icon ?>
			<?php echo $label ?>
		</a>
	<?php endif; ?>

</div>

<div class="clear"></div>