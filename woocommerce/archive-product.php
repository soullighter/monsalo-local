<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit; ?>



	<?php get_header( 'shop' );	

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
add_action( 'monsalo_woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
do_action( 'monsalo_woocommerce_before_main_content' );
// do_action( 'woocommerce_before_main_content' );

?>


<div class="home_top_section">
	<div class="container">
		<div class="row">
			<div class="home_top_content col-lg-6 col-xl-5 align-items-center d-flex flex-column justify-content-center">
				<h1 class="home_top_title">become a member and save up to 80% on more than 600 top brands</h1>
				<div class="home_top_subtitle">membership benefits</div>
				<div class="benefits_wrap d-flex justify-content-around w-100">
					<div class="benefit">
						<img src="<?php echo get_template_directory_uri(); ?>/img/box-delivery.svg" alt="icon">
						<div>Cheap Delivery</div>
					</div>
					<div class="benefit">
						<img src="<?php echo get_template_directory_uri(); ?>/img/percent.svg" alt="">
						<div>Big Savings</div>
					</div>
					<div class="benefit">
						<img src="<?php echo get_template_directory_uri(); ?>/img/cashback.svg" alt="">
						<div>Earn BonusCash</div>
					</div>
				</div>
				<div class="home_top_text">Luxplus is a membership club that offers huge savings on thousands of awesome products. First month free (hereafter 8.99/mo.). And you can cancel your membership whenever you want.</div>
				<a class="home_top_link" href="#">See how much you can save</a>
				<a class="home_top_link" href="#">Read more about DistPrice</a>
			</div>
			<div class="home_top_img_wrap">
				<img src="<?php echo get_template_directory_uri(); ?>/img/home_top_bg.png" alt="img">
			</div>
		</div>
	</div>
</div>


<?php 

	
if ( is_front_page() ) {
	# code...
} else {  ?>

	<header class="woocommerce-products-header container">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
		<?php endif; ?>

		<?php
		/**
		 * Hook: woocommerce_archive_description.
		 *
		 * @hooked woocommerce_taxonomy_archive_description - 10
		 * @hooked woocommerce_product_archive_description - 10
		 */
		do_action( 'woocommerce_archive_description' );
		?>
	</header>
<?php } ?>

<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	echo '<div class="container clearfix filter-product"> <div class="col-12">';
		
		do_action( 'woocommerce_before_shop_loop' );
	echo "</div></div>";
	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
?>

	<div class="additional_filters container">
		<div class="row add_filters_wrap d-flex justify-content-center align-content-center flex-wrap">
			<div class="d-flex justify-content-center align-content-center flex-wrap" style="width: 100%;">
				<?php if ( have_rows( 'products_list', 'option' ) ) : ?>
								<?php while ( have_rows( 'products_list', 'option' ) ) : the_row(); ?>
									
									<button class="additional_filter_btn">
									<?php $catalog_type_term = get_sub_field( 'catalog_type' ); ?>
										<a href="<?php echo get_term_link( $catalog_type_term ); ?>"><?php echo $catalog_type_term->name; ?></a>
									</button>
								<?php endwhile; ?>
							
							<!-- if products_list end -->
							<?php endif; ?>
			</div>
		</div>
	</div>
<div class="best_selling_section">
	<div class="container">
		<div class="best_selling_title_wrap d-flex justify-content-between align-items-center flex-wrap">
			<div class="best_selling_title"><?php esc_html_e( 'Best Selling Brands', 'woocommerce' ); ?></div>
		</div>
		<div class="row">
	<?php woocommerce_product_loop_start(); ?>
		<?php
		$args = array(
		    'post_type' => 'product',
		    'meta_key' => 'total_sales',
		    'orderby' => 'meta_value_num',
		    'posts_per_page' => 4,
		);
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
		global $product; 
		wc_get_template_part( 'content', 'product' );
		?>
		
		<?php endwhile; ?>
		<?php wp_reset_query(); ?>

		<?php woocommerce_product_loop_end(); ?>	
		</div>
	</div>
</div>
<?php wp_reset_postdata(); ?>


<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
// do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
