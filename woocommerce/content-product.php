<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

			<li <?php wc_product_class( 'prod_card_wrap col-xl-3 col-lg-4 col-md-6', $product );   ?>>

			<div class="prod_card h-100 d-flex flex-column">
						<div class="d-flex-product">
							<div class="prod_card_price d-flex align-items-center justify-content-center">
							<?php
								$regular_price  = get_post_meta( $product->get_id(), '_regular_price', true );
								$sale_perc = 20;
								$member_calc_price = ($sale_perc / 100) * $regular_price ;
								$memberprice_display =  $regular_price - $member_calc_price;
								$symbol = get_woocommerce_currency_symbol( $args['currency'] );
								echo $symbol . ' ' . number_format($memberprice_display, 2, '.', '');
							?>
							</div>
							<div class=" prod_card_btn center">
							<?php
							/**
							* Hook: woocommerce_after_shop_loop_item.
							*
							* @hooked woocommerce_template_loop_product_link_close - 5
							* @hooked woocommerce_template_loop_add_to_cart - 10
							*/
							do_action( 'woocommerce_after_shop_loop_item' );
	?>
							</div>
							
						</div>
						<div class="prod_card_content d-flex flex-column flex-grow-1">
							<div class="prod_card_title flex-grow-1">

								<?php

								/**
								 * Hook: woocommerce_before_shop_loop_item.
								 *
								 * @hooked woocommerce_template_loop_product_link_open - 10
								 */
								do_action( 'woocommerce_before_shop_loop_item' );
									
								/**
								* Hook: woocommerce_shop_loop_item_title.
								*
								* @hooked woocommerce_template_loop_product_title - 10
								*/
								do_action( 'woocommerce_shop_loop_item_title' );
								?>
							</div>
							<div class="prod_card_img d-flex align-items-center justify-content-center">
							<?php
							/**
							* Hook: woocommerce_before_shop_loop_item_title.
							*
							* @hooked woocommerce_show_product_loop_sale_flash - 10
							* @hooked woocommerce_template_loop_product_thumbnail - 10
							*/
							do_action( 'woocommerce_before_shop_loop_item_title' );
							?>
						</div>
							<div class="d-flex align-items-center justify-content-between">


									<div class="prod_card_nprice">
										Normal Price  
										<?php
											// Var setup above where is Member price calc
											echo $symbol . ' ' . $regular_price;
											// $price_html = $product->get_price_html();
											// echo $price_html;
										?>
										</div>
							</div>
						</div>
						<div class="prod_card_info d-flex align-items-center justify-content-between">
						<?php 
								
								// if ( class_exists( 'Obj_Main' ) ):
  						// 		$MyFoo = new Obj_Main();
  						// 	endif;
								// $MyFoo->cgd_add_product_heart();
							echo do_shortcode(  '[yith_wcwl_add_to_wishlist]' );
						 ?>
						  <!-- 135 -->

							<div class="prod_card_info_item d-flex justify-content-center">
								<svg class="svg-inline--fa fa-info-circle fa-w-16 info_icon" data-toggle="tooltip" data-placement="bottom" title="*Not charged now. This will appear on your monthly invoice once order is completely submitted for processing." aria-labelledby="svg-inline--fa-title-4FsPkX9l9Z10" data-prefix="fas" data-trigger="click" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg><!-- <i class="fas fa-info-circle info_icon" data-toggle="tooltip" data-placement="bottom" title="*Not charged now. This will appear on your monthly invoice once order is completely submitted for processing."></i> -->
							</div>
							<div class="prod_card_info_item d-flex justify-content-end">
								<img class="delivery_icon" src="<?php echo get_template_directory_uri(); ?>/img/delivery.svg" alt="img" data-toggle="tooltip" data-placement="bottom" title="Deliver info" data-trigger="click">
							</div>
						</div>
					</div>
				<?php

				?>
			</li>
