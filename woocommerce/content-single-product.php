<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );
do_action( 'woocommerce_before_single_product ' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>"
   <?php wc_product_class( 'single-product-page product-info container', $product ); ?>>

   <div>
      <?php 
            add_action( 'monsalo_wc_product_header', 'woocommerce_output_all_notices', 0 );
				add_action('monsalo_wc_product_header', 'woocommerce_breadcrumb', 5);
				add_action('monsalo_wc_product_header', 'woocommerce_template_single_title', 10);
				add_action( 'monsalo_wc_product_header', 'woocommerce_template_single_rating', 15 );
				do_action( 'monsalo_wc_product_header' );
         ?>
         

   </div>

   <div class="product-img-wrap clearfix">
      <?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
   do_action( 'woocommerce_before_single_product_summary' );
   ?>


   </div>


   <div class="summary entry-summary">
   <!-- <a href="#" class="btn-save">Save $5</a> -->
      <?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		function monsalo_wc_regular_price() {
			?>

      <div class="product-price-wrap">
         <div class="single-product-price">
            <label class="container-price active">
               <?php
                  global $product;
                  $regular_price  = get_post_meta( $product->get_id(), '_regular_price', true );

                  $sale_perc = 20;
                  $member_calc_price = ($sale_perc / 100) * $regular_price ;
                  $memberprice_display =  $regular_price - $member_calc_price;
                  $symbol = get_woocommerce_currency_symbol( $args['currency'] );

                  echo $symbol . ' ' . number_format($memberprice_display, 2, '.', '');
               ?>
               <br><span>Member price</span>
               <input type="checkbox" checked="checked">
               <span class="checkmark"></span>
            </label>
            <label class="container-price">
               <?php echo $symbol . ' ' . number_format($regular_price, 2, '.', ''); ?>
               <br><span>Normal price</span>
               <input type="checkbox">
               <span class="checkmark"></span>
            </label>
         </div>

         <?php 
            // wc-memberships-member-discount-message
            add_action( 'monsalo_wc_member_message', array( 'WC_Memberships_Restrictions', 'single_product_member_discount_message' ), 31 );
            do_action( 'monsalo_wc_member_message' );
          ?>

      </div>

      <?php
		}
            
       
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
      remove_action( 'woocommerce_single_product_summary', array( 'WC_Memberships_Restrictions', 'single_product_member_discount_message' ), 31 );




      // add_action( 'woocommerce_single_product_summary', 'monsalo_wc_regular_price', 15  );
      
      ?>

      <?php
		do_action( 'woocommerce_single_product_summary' );
		?>
      <!-- </div> -->
      

   </div>
   <?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	?>

   <?php
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20  );
do_action( 'woocommerce_after_single_product_summary' );

 ?>
</div>
<?php 
	do_action( 'woocommerce_after_single_product' ); 

	add_action( 'monsalo_wc_output_related_products', 'woocommerce_output_related_products', 20  );
	do_action( 'monsalo_wc_output_related_products' );
?>
